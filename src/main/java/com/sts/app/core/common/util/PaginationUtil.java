package com.sts.app.core.common.util;

import com.sts.app.core.common.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class PaginationUtil {

    private PaginationUtil(){

    }

    public static Pageable constructPageSpecification(Page page) {
        Pageable pageSpecification = new PageRequest(page.getPageNumber() - 1, page.getSize(), sortBy(page.getSortBy(), page.isAscending()));
        return pageSpecification;
    }

    private static Sort sortBy(String sortByProperty, boolean ascending) {
        if(ascending){
            return new Sort(Sort.Direction.ASC, sortByProperty);
        } else {
            return new Sort(Sort.Direction.DESC, sortByProperty);
        }
    }
}
