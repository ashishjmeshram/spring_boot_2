package com.sts.app.core.common.domain;

public class Page {

    public static final int DEFAULT_PAGE_SIZE = 10;
    public static final String DEFAULT_SORT_BY_PROPERTY = "createdDate";

    private int pageNumber = 1;
    private int size = DEFAULT_PAGE_SIZE;
    private boolean ascending = true;
    private String sortBy = DEFAULT_SORT_BY_PROPERTY;

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isAscending() {
        return ascending;
    }

    public void setAscending(boolean ascending) {
        this.ascending = ascending;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }
}
