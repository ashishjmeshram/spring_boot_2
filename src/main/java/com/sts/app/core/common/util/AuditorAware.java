package com.sts.app.core.common.util;

import com.sts.app.core.config.profile.Dev;
import com.sts.app.core.config.profile.Prod;
import com.sts.app.core.user.security.SecurityUtil;
import org.springframework.stereotype.Component;

@Dev
@Prod
@Component("auditorAware")
public class AuditorAware implements org.springframework.data.domain.AuditorAware<Long> {

    /**
     * Returns the current auditor of the application.
     *
     * @return the current auditor
     */
    @Override
    public Long getCurrentAuditor() {
        return SecurityUtil.getCurrentUser().getId();
    }
}
