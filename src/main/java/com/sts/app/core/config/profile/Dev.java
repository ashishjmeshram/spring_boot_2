package com.sts.app.core.config.profile;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.sts.app.core.common.util.Constants;
import org.springframework.context.annotation.Profile;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Profile(Dev.NAME)
public @interface Dev {
	String NAME = Constants.SPRING_PROFILE_DEVELOPMENT;
}