package com.sts.app.core.user.web;

import com.sts.app.core.common.domain.Page;
import com.sts.app.core.common.util.PaginationUtil;
import com.sts.app.core.user.security.SecurityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sts.app.core.user.domain.User;
import com.sts.app.core.user.service.UserService;
import com.sts.app.core.common.util.WebUtil;

@Controller
@RequestMapping(value = { "/user"})
public class UserController {

	public static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping(params = "operation=userList")
	public String userList(Model model, Page page) {
		org.springframework.data.domain.Page<User> userPage = userService.findUsersForPage(PaginationUtil.constructPageSpecification(page));
		model.addAttribute("userPage", userPage);
		return "userList";
	}

	@RequestMapping(params = "operation=newUser")
	public String newUser(Model model) {
		return "userForm";
	}

	@RequestMapping(params = "operation=editUser")
	public String editUser(Long userId, Model model) {
		model.addAttribute("user", userService.findUserById(userId));
		return "userForm";
	}

	@RequestMapping(params = "operation=saveUser")
	public String saveUser(User user, Model model) {
		userService.saveUser(user);
		return WebUtil.createRedirectViewPath("/user?operation=userList");
	}

	@RequestMapping(params = "operation=deleteUser")
	public String deleteUser(Long userId, Model model) {
		userService.deleteUser(userId);
		return WebUtil.createRedirectViewPath("/user?operation=userList");
	}

}
