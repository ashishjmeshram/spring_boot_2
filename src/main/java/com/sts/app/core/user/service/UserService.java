package com.sts.app.core.user.service;

import java.util.List;

import com.sts.app.core.user.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

	List<User> findAllUsers();

	Page<User> findUsersForPage(Pageable pageable);

	User findUserById(Long id);

	User findUserByEmail(String email);

	void saveUser(User user);

	void deleteUser(Long userId);
}
