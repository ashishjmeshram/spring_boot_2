package com.sts.app.core.user.repository;

import com.sts.app.core.common.repository.BaseRepository;
import com.sts.app.core.user.domain.User;

public interface UserRepository extends BaseRepository<User, Long> {

    User findByEmail(String email);

}
