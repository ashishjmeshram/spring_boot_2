
package com.sts.app.core.user.domain;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "user_role_assoc")
public class UserRole implements Serializable {

    private static final long serialVersionUID = 534969206983504660L;
    
    @Id
    @Column(name = "user_role_id", nullable = false)
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String userRoleId;
    
    @Column(name = "role_id")
    private Integer roleId;
    
    @Column(name="user_id")
    private String userId;
    
    @Column(name = "is_enabled")
    private boolean enabled = true;
    
    public String getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(String userRoleId) {
        this.userRoleId = userRoleId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
