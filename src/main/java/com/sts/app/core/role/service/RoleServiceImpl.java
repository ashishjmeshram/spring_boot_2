package com.sts.app.core.role.service;

import com.sts.app.core.role.domain.Role;
import com.sts.app.core.role.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service("roleService")
@Transactional(readOnly = true)
public class RoleServiceImpl implements RoleService {

	private static final Logger log = LoggerFactory.getLogger(RoleServiceImpl.class);

	@Autowired
	private RoleRepository roleRepository;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackForClassName = {"java.lang.Exception" })
	public void saveRole(Role role) {
		log.debug("Saving a role with the information: {}", role);
		
		boolean isRoleNew = role.getRoleId() == null || role.getRoleId() == 0;
		
		if(isRoleNew){
			roleRepository.save(role);
		} else {
			Role dbRole = roleRepository.findOne(role.getRoleId());
			
			dbRole.setTitle(role.getTitle());
			dbRole.setDisplayName(role.getDisplayName());
			dbRole.setDescription(role.getDescription());
			dbRole.setEnabled(role.isEnabled());

			roleRepository.save(dbRole);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackForClassName = {"java.lang.Exception" })
	public void deleteRole(Long roleId) {
		log.debug("Deleting a role entry with the role id: {}", roleId);
		Role role = roleRepository.findOne(roleId);
		roleRepository.delete(role);
	}

	@Override
	public Role findRoleById(Long id) {
		log.debug("Finding a role entry with the role id: {}", id);
		return roleRepository.findOne(id);
	}

	@Override
	public List<Role> findAllRoles() {
		log.debug("Finding all role entries");
		return (List<Role>) roleRepository.findAll();
	}
}
