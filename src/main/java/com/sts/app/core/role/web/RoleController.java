package com.sts.app.core.role.web;

import com.sts.app.core.common.util.WebUtil;
import com.sts.app.core.role.domain.Role;
import com.sts.app.core.role.service.RoleService;
import com.sts.app.core.user.security.SecurityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = { "/role"})
public class RoleController {

	public static final Logger logger = LoggerFactory.getLogger(RoleController.class);

	@Autowired
	private RoleService roleService;

	@RequestMapping(params = "operation=roleList")
	public String roleList(Model model) {
		model.addAttribute("roleList", roleService.findAllRoles());
		return "roleList";
	}

	@RequestMapping(params = "operation=newRole")
	public String newRole(Model model) {
		return "roleForm";
	}

	@RequestMapping(params = "operation=editRole")
	public String editRole(Long roleId, Model model) {
		model.addAttribute("role", roleService.findRoleById(roleId));
		return "roleForm";
	}

	@RequestMapping(params = "operation=saveRole")
	public String saveRole(Role role, Model model) {
		roleService.saveRole(role);
		return WebUtil.createRedirectViewPath("/role?operation=roleList");
	}

	@RequestMapping(params = "operation=deleteRole")
	public String deleteRole(Long roleId, Model model) {
		roleService.deleteRole(roleId);
		return WebUtil.createRedirectViewPath("/role?operation=roleList");
	}
}
