
package com.sts.app.core.role.domain;

import com.sts.app.core.module.domain.ModuleOperation;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "role_module_operation_assoc")
public class RoleModuleOperation implements Serializable {

    private static final long serialVersionUID = 6236369194306737681L;
    
    @Id
    @Column(name = "role_module_operation_id", nullable = false)
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String roleModuleOperationId;
    
    @Column(name = "role_id")
    private Integer roleId;
    
    @Column(name="module_operation_id")
    private Integer moduleOperationId;
    
    @OneToOne
    @JoinColumn(name="module_operation_id",insertable=false,updatable=false)
    private ModuleOperation moduleOperation;

    public String getRoleModuleOperationId() {
        return roleModuleOperationId;
    }

    public void setRoleModuleOperationId(String roleModuleOperationId) {
        this.roleModuleOperationId = roleModuleOperationId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getModuleOperationId() {
        return moduleOperationId;
    }

    public void setModuleOperationId(Integer moduleOperationId) {
        this.moduleOperationId = moduleOperationId;
    }

    public ModuleOperation getModuleOperation() {
        return moduleOperation;
    }

    public void setModuleOperation(ModuleOperation moduleOperation) {
        this.moduleOperation = moduleOperation;
    }
}
