package com.sts.app.core.role.service;

import com.sts.app.core.role.domain.Role;

import java.util.List;

public interface RoleService {

	void saveRole(Role role);

	void deleteRole(Long roleId);

	Role findRoleById(Long id);

	List<Role> findAllRoles();
}
