
package com.sts.app.core.role.domain;

import com.sts.app.core.common.domain.AbstractAuditingEntity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Role extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 5354017907353198316L;
    
    @Id
    @Column(name = "role_id", nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long roleId;
    
    @Column(name = "title", nullable = false)
    private String title;
    
    @Column(name = "display_name", nullable = false)
    private String displayName;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "is_enabled")
    private boolean enabled = true;
    
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="role_id")
	private List<RoleModuleOperation> roleModuleOperationList;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<RoleModuleOperation> getRoleModuleOperationList() {
        return roleModuleOperationList;
    }

    public void setRoleModuleOperationList(List<RoleModuleOperation> roleModuleOperationList) {
        this.roleModuleOperationList = roleModuleOperationList;
    }
}
