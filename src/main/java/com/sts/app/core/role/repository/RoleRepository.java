package com.sts.app.core.role.repository;

import com.sts.app.core.common.repository.BaseRepository;
import com.sts.app.core.role.domain.Role;

public interface RoleRepository extends BaseRepository<Role, Long> {

}
