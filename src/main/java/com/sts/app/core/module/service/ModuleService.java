package com.sts.app.core.module.service;

import com.sts.app.core.module.domain.Module;
import com.sts.app.core.module.domain.ModuleOperation;

import java.util.List;

public interface ModuleService {

	List<Module> findAllModules();

	List<ModuleOperation> findAllModuleOperations(String moduleName);

	List<ModuleOperation> findModuleOperationsByRoleId(Integer roleId);
}
