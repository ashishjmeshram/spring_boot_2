
package com.sts.app.core.module.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "modules")
public class Module implements Serializable {

    private static final long serialVersionUID = -19027628511627182L;
    
    @Id
    @Column(name = "module_id", nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer moduleId;
    
    @Column(name = "module_name", nullable = false)
    private String moduleName;
    
    @Column(name = "display_name", nullable = false)
    private String displayName;
    
    @OneToMany(fetch=FetchType.EAGER)
    @JoinColumn(name = "module_id")
    private List<ModuleOperation> moduleOperationList;

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<ModuleOperation> getModuleOperationList() {
        return moduleOperationList;
    }

    public void setModuleOperationList(List<ModuleOperation> moduleOperationList) {
        this.moduleOperationList = moduleOperationList;
    }
}
