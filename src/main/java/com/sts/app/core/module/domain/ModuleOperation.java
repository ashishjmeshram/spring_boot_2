
package com.sts.app.core.module.domain;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "module_operations")
public class ModuleOperation implements Serializable {

    private static final long serialVersionUID = 4879958589936398227L;

    @Id
    @Column(name = "module_operation_id", nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer moduleOperationId;

    @Column(name = "module_operation_name", nullable = false)
    private String moduleOperationName;

    @Column(name = "display_name", nullable = false)
    private String displayName;

    @Column(name = "module_id", nullable = false)
    private Integer moduleId;

    public Integer getModuleOperationId() {
        return moduleOperationId;
    }

    public void setModuleOperationId(Integer moduleOperationId) {
        this.moduleOperationId = moduleOperationId;
    }

    public String getModuleOperationName() {
        return moduleOperationName;
    }

    public void setModuleOperationName(String moduleOperationName) {
        this.moduleOperationName = moduleOperationName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }
}