package com.sts.app.core.module.repository;

import com.sts.app.core.common.repository.BaseRepository;
import com.sts.app.core.module.domain.ModuleOperation;
import com.sts.app.core.role.domain.RoleModuleOperation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ModuleOperationRepository extends BaseRepository<ModuleOperation, Integer> {

    List<ModuleOperation> findByModuleId(Integer moduleId);

    @Query("SELECT DISTINCT(mo) FROM ModuleOperation mo, RoleModuleOperation rmo where mo.moduleOperationId = rmo.moduleOperationId and rmo.roleId = :roleId")
    List<ModuleOperation> findByRoleId(@Param("roleId") Integer roleId);
}
