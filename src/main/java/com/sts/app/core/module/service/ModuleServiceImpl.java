package com.sts.app.core.module.service;

import java.util.List;

import com.sts.app.core.module.domain.Module;
import com.sts.app.core.module.domain.ModuleOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("moduleService")
public class ModuleServiceImpl implements ModuleService{
	@Override
	public List<Module> findAllModules() {
		return null;
	}

	@Override
	public List<ModuleOperation> findAllModuleOperations(String moduleName) {
		return null;
	}

	@Override
	public List<ModuleOperation> findModuleOperationsByRoleId(Integer roleId) {
		return null;
	}
	/*
	@Autowired
	private ModuleDAO moduleDAO;
	
	public List<Module> findAllModules() {
		return moduleDAO.findAllModules();
	}

	public List<String> getAllModuleOperations(String moduleName, boolean observer) {
		Module module = findModuleByName(moduleName);
		if(module!=null){
			return getAllModuleOperations(module.getModuleId(),observer);
		}
		return null;
	}

	public List<ModuleOperation> getModuleOperationsForRole(Integer roleId) {
		return moduleDAO.getModuleOperationsForRole(roleId);
	}

	public Module findModuleByName(String moduleName) {
		return moduleDAO.findModuleByName(moduleName);
	}

	public List<String> getAllModuleOperations(int moduleId, boolean observer) {
		return moduleDAO.getAllModuleOperations(moduleId,observer);
	}*/
}
