package com.sts.app.core.module.repository;

import com.sts.app.core.common.repository.BaseRepository;
import com.sts.app.core.module.domain.Module;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ModuleRepository extends BaseRepository<Module, Integer> {

    @Query("SELECT m FROM Module m ORDER BY m.moduleName ASC")
    List<Module> findAllModules();

}
