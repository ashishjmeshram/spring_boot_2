<%@ include file="../common/taglibs.jsp"%>

<script>
	$(document).ready(function(){
	    var grid = $("#role-grid").bootgrid({
	        selection: true,
	        multiSelect: true,
	        rowSelect: true,
	        keepSelection: true,
	        formatters: {
	        	"commands": function(column, row) {
	                return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + row.id + "\"><span class=\"fa glyphicon glyphicon-pencil\"></span></button> " + 
	                    "<button type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"" + row.id + "\"><span class=\"fa glyphicon glyphicon-trash\"></span></button>";
	            }
	        }
	    }).on("selected.rs.jquery.bootgrid", function(e, rows){
	        var rowIds = [];
	        for (var i = 0; i < rows.length; i++){
	            rowIds.push(rows[i].id);
	        }
	    }).on("deselected.rs.jquery.bootgrid", function(e, rows){
	        var rowIds = [];
	        for (var i = 0; i < rows.length; i++){
	            rowIds.push(rows[i].id);
	        }
	    }).on("loaded.rs.jquery.bootgrid", function(){
	    	
		    grid.find(".command-edit").on("click", function(e){
		    	var roleId = $(this).data("row-id");
		    	var editURL = "<c:url value='/role?operation=editRole'/>&roleId="+ roleId;	
		    	commonObj.goToURL(editURL);
		    }).end().find(".command-delete").on("click", function(e){
		    	var roleId = $(this).data("row-id");
		    	var deleteURL = "<c:url value='/role?operation=deleteRole'/>&roleId="+ roleId;	

		    	bootbox.confirm("Are you sure you want to delete this role?", function(result) {
		    		if(result){
				    	commonObj.goToURL(deleteURL); 
		    		}
		    	});
		    });
		});
	    
	});

</script>

<table id="role-grid" class="table table-condensed table-hover table-striped">
    <thead>
        <tr>
            <th data-column-id="id" data-type="numeric" data-identifier="true"><fmt:message key="role.id"/></th>
            <th data-column-id="title"><fmt:message key="role.title"/></th>
            <th data-column-id="displayName"><fmt:message key="role.displayName"/></th>
            <th data-column-id="description"><fmt:message key="role.description"/></th>
            <th data-column-id="commands" data-formatter="commands" data-sortable="false"><fmt:message key="actions"/></th>
        </tr>
    </thead>
    <tbody>
    	<c:forEach var="role" items="${roleList}">
    		<tr>
				<td>${role.roleId}</td>
    			<td>${role.title}</td>
    			<td>${role.displayName}</td>
    			<td>${role.description}</td>
    			<td></td>
    		</tr>
    	</c:forEach>
    </tbody>
</table>

<input class="btn btn-primary" type="button" value="<fmt:message key='role.addNew'/>" onclick="javascript:commonObj.goToURL('<c:url value='/role?operation=newRole'/>');">

	
