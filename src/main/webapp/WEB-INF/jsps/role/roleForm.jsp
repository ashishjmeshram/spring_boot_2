<%@ include file="../common/taglibs.jsp"%>

<form name="createRoleForm" id="createRoleForm" data-toggle="validator" class="form-horizontal" action="<c:url value='/role?operation=saveRole'/>" method="POST">

	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" name="roleId" value="${ role.roleId }" />
	
	<div class="row">
		<div class="">
			<div class="panel panel-primary">
			
				<div class="panel-heading">
					<h3 class="panel-title">
					
					<c:choose>
						<c:when test="${ role eq null or role.roleId eq null or role.roleId =='' }">
							<fmt:message key="role.add"/>
						</c:when>
						<c:otherwise>
							<fmt:message key="role.edit"/>
						</c:otherwise>
					</c:choose>
					
					</h3>
				</div>
			
				<div class="panel-body">
				
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label"><fmt:message key="role.title"/></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="title" name="title" value="${ role.title }" placeholder="<fmt:message key="role.title"/>" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>

					<div class="form-group">
						<label for="displayName" class="col-sm-2 control-label"><fmt:message key="role.displayName"/></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="displayName" name="displayName" value="${ role.displayName }" placeholder="<fmt:message key="role.displayName"/>" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					
					<div class="form-group">
						<label for="description" class="col-sm-2 control-label"><fmt:message key="role.description"/></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="description" name="description" value="${ role.description }" placeholder="<fmt:message key="role.description"/>">
							<div class="help-block with-errors"></div>
						</div>
					</div>

				</div>
				<div class="panel-footer clearfix">
					<div class="pull-right">
						<button type="submit" class="btn btn-primary"><fmt:message key="save"/></button>
						<button type="button" class="btn btn-default" onclick="javascript:commonObj.goToURL('<c:url value='/role?operation=roleList'/>');" ><fmt:message key="cancel"/></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
