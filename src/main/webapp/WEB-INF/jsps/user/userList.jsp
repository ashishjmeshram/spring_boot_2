<%@ include file="../common/taglibs.jsp"%>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>

<script>

    function User(){

        this.editUser = function(userId){
            var editURL = "<c:url value='/user?operation=editUser'/>&userId="+ userId;
            commonObj.goToURL(editURL);
        };

        this.deleteUser = function(userId){
            var deleteURL = "<c:url value='/user?operation=deleteUser'/>&userId="+ userId;

            bootbox.confirm("Are you sure you want to delete this user?", function(result) {
                if(result){
                    commonObj.goToURL(deleteURL);
                }
            });
        };

    }

    var userObj = new User();

	$(document).ready(function(){

	});

</script>

<table id="user-grid" class="table table-condensed table-hover table-striped">
    <thead>
        <tr>
            <th><fmt:message key="user.id"/></th>
            <th class=""><fmt:message key="user.firstname"/></th>
            <th><fmt:message key="user.lastname"/></th>
            <th><fmt:message key="user.email"/></th>
            <th><fmt:message key="actions"/></th>
        </tr>
    </thead>
    <tbody>
    	<c:forEach var="user" items="${userPage.content}">
    		<tr>
    			<td>${user.id}</td>
    			<td>${user.firstname}</td>
    			<td>${user.lastname}</td>
    			<td>${user.email}</td>
    			<td>
                    <button type="button" class="btn btn-xs btn-default command-edit">
                        <span class="fa glyphicon glyphicon-pencil" onclick="javascript:userObj.editUser('${user.id}');"></span>
                    </button>
                    <button type="button" class="btn btn-xs btn-default command-delete">
                        <span class="fa glyphicon glyphicon-trash" onclick="javascript:userObj.deleteUser('${user.id}');"></span>
                    </button>
                </td>
    		</tr>
    	</c:forEach>
    </tbody>
</table>

<util:pagination page="${ userPage }" pageUrl="user?operation=userList" />

<input class="btn btn-primary" type="button" value="<fmt:message key='user.addNew'/>" onclick="javascript:commonObj.goToURL('<c:url value='/user?operation=newUser'/>');">

	
	