<!DOCTYPE html>

<%@ include file="../common/taglibs.jsp"%>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Simple Gradle Application</title>

	<!-- CSS files goes below -->
    <link rel="stylesheet" href="<c:out value='static/css/bootstrap.min.css'/>" >
	<link rel="stylesheet" href="<c:out value='static/css/layout.css'/>" />
	
	<!-- Javascript files goes below  -->
	<script type="text/javascript" src="<c:out value='static/js/jquery-1.11.3.min.js'/>"></script>
    <script type="text/javascript" src="<c:out value='static/js/bootstrap.min.js' />"></script>
	<script type="text/javascript" src="<c:out value='static/js/validator.min.js'/>"></script>
	<script type="text/javascript" src="<c:out value='static/js/common.js'/>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
	<div class="container-fluid">
		<tiles:insertAttribute name="header" />
		<tiles:insertAttribute name="body" />
		<tiles:insertAttribute name="footer" />
	</div>

  </body>
</html>