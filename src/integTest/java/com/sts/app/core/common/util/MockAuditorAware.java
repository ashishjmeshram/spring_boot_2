package com.sts.app.core.common.util;

import com.sts.app.core.config.profile.Test;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

@Test
@Component("auditorAware")
public class MockAuditorAware implements AuditorAware<Long> {

    /**
     * Returns the current auditor of the application.
     *
     * @return the current auditor
     */
    @Override
    public Long getCurrentAuditor() {
        return 1000L;
    }
}
