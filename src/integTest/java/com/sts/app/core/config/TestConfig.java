package com.sts.app.core.config;

import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.destination.Destination;
import com.sts.app.core.config.profile.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

import javax.sql.DataSource;

@Test
@Configuration
public class TestConfig {

	@Bean
	@Autowired
	public Destination destination(DataSource dataSource) {
		return new DataSourceDestination(dataSource);
	}

}
